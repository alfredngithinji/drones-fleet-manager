package com.musala.fleetmanager.drones.services;

import com.musala.fleetmanager.drones.dto.*;
import com.musala.fleetmanager.drones.mappers.DroneMapper;
import com.musala.fleetmanager.drones.models.Drone;
import com.musala.fleetmanager.drones.models.DroneBatteryLevel;
import com.musala.fleetmanager.drones.models.Medication;
import com.musala.fleetmanager.drones.models.MedicationDispatchItem;
import com.musala.fleetmanager.drones.repository.DroneBatteryLevelRepository;
import com.musala.fleetmanager.drones.repository.DroneRepository;
import com.musala.fleetmanager.drones.repository.MedicationDispatchItemRepository;
import com.musala.fleetmanager.drones.repository.MedicationRepository;
import com.musala.fleetmanager.drones.utils.DroneModelEnum;
import com.musala.fleetmanager.drones.utils.DroneStateEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class DroneServiceImpl implements DroneService {


    private final DroneRepository droneRepository;

    private final DroneMapper droneMapper;

    private final DroneBatteryLevelRepository droneBatteryLevelRepository;

    private final Integer minimalBatteryLevel = 25;

    private final MedicationRepository medicationRepository;

    private  final MedicationDispatchItemRepository medicationDispatchItemRepository;


    @Override
    public DroneRegistrationResponse droneRegistration(DroneRegistrationRequest droneRegistrationRequest) {

        DroneRegistrationResponse droneRegistrationResponse = new DroneRegistrationResponse();

        if(!isModel(droneRegistrationRequest.getModel())){
            droneRegistrationResponse.setMessage("Invalid model");
            droneRegistrationResponse.setCreateAt(LocalDateTime.now());
            droneRegistrationResponse.setResult("Failed");
            return droneRegistrationResponse;
        }
        if(!isCode(droneRegistrationRequest.getState())){
            droneRegistrationResponse.setMessage("Invalid State");
            droneRegistrationResponse.setCreateAt(LocalDateTime.now());
            droneRegistrationResponse.setResult("Failed");
            return droneRegistrationResponse;
        }

        Drone drone = new Drone();
        drone.setSerialNumber(droneRegistrationRequest.getSerialNumber());
        drone.setWeight(droneRegistrationRequest.getWeight());
        drone.setModel(DroneModelEnum.valueOf(droneRegistrationRequest.getModel()));
        drone.setBatteryCapacity(droneRegistrationRequest.getBatteryCapacity());

        drone.setState(DroneStateEnum.valueOf(droneRegistrationRequest.getState()));
        droneRepository.save(drone);


        droneRegistrationResponse.setSerialNumber(drone.getSerialNumber());
        droneRegistrationResponse.setMessage("Drone was successively created");
        droneRegistrationResponse.setCreateAt(LocalDateTime.now());
        droneRegistrationResponse.setResult("Success");

        return droneRegistrationResponse;
    }


    public DroneLoadResponse loadDrone(DroneLoadRequest droneLoadRequest) {
        Drone drone = droneRepository.findBySerialNumber(droneLoadRequest.getSerialNumber()).orElseThrow(() -> new IllegalArgumentException("Invalid drone id"));
        DroneLoadResponse droneLoadResponse = new DroneLoadResponse();
        droneLoadResponse.setSerialNumber(droneLoadRequest.getSerialNumber());

        if (!checkDroneStatus(drone)) {
            droneLoadResponse.setResult("Failed");
            droneLoadResponse.setMessage("The Drone is not idle or loading");
        }


        if (!checkDroneBatteryLevel(drone)) {
            droneLoadResponse.setResult("Failed");
            droneLoadResponse.setMessage("Drone battery capacity is below required level");
        }

        if (!checkMedication(droneLoadRequest.getCode())) {
            droneLoadResponse.setResult("Failed");
            droneLoadResponse.setMessage("Medication is not available");
        }
        Medication medication = medicationRepository.findByCode(droneLoadRequest.getCode()).orElseThrow(() -> new IllegalArgumentException("Invalid medication code"));


        if (drone.getWeight() < medication.getWeight()) {
            droneLoadResponse.setResult("Failed");
            droneLoadResponse.setMessage("Weight is more than required");
        }

        MedicationDispatchItem medicationDispatchItem = new MedicationDispatchItem();
        medicationDispatchItem.setMedication(medication);
        medicationDispatchItem.setDrone(drone);
        medicationDispatchItem.setSource(droneLoadRequest.getSource());
        medicationDispatchItem.setDestination(droneLoadRequest.getDestination());
        medicationDispatchItemRepository.save(medicationDispatchItem);

        drone.setState( DroneStateEnum.LOADING);
        droneRepository.save(drone);
        droneLoadResponse.setResult("Success");
        droneLoadResponse.setMessage("Drone was successively created");
        droneLoadResponse.setSerialNumber (LocalDateTime.now().toString()); ;

        return droneLoadResponse;

    }


    @Override
    public MedicationDispatchItemResponse addMedicationDispatchItem(MedicationDispatchItemRequest medicationDispatchItemRequest) {

        Drone drone = droneRepository.
                findById(medicationDispatchItemRequest.getDroneId()).
                orElseThrow(() -> new IllegalArgumentException("Invalid drone id"));


        return null;
    }


    private boolean checkMedication(String code) {
        Optional<Medication> medication = medicationRepository.findByCode(code);
        return medication.isPresent();
    }

    private boolean checkDroneStatus(Drone drone) {
        return drone.getState().name().equals(DroneStateEnum.IDLE.name()) ||
                drone.getState().name().equals(DroneStateEnum.LOADING.name());
    }

    private Float checkTotalWeightLoaded(Drone drone) {
        return 0f;
    }

    private boolean checkDroneBatteryLevel(Drone drone) {
       // DroneBatteryLevel droneBatteryLevel = droneBatteryLevelRepository.findFirstByDroneOrderByIdDesc(drone);
        return drone.getBatteryCapacity()> minimalBatteryLevel;
    }

    public boolean isModel(String value) {
        return Arrays.stream(DroneModelEnum.values()).anyMatch(e -> e.name().equals(value));
    }

    public boolean isCode(String value){
        return Arrays.stream(DroneStateEnum.values()).anyMatch(e -> e.name().equals(value));
    }


}
