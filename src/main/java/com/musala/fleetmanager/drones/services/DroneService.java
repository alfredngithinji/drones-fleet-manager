package com.musala.fleetmanager.drones.services;

import com.musala.fleetmanager.drones.dto.*;

public interface DroneService {
     DroneRegistrationResponse droneRegistration(DroneRegistrationRequest droneRegistrationRequest);

     MedicationDispatchItemResponse addMedicationDispatchItem (MedicationDispatchItemRequest medicationDispatchItemRequest);

     DroneLoadResponse loadDrone(DroneLoadRequest droneLoadRequest);
}
