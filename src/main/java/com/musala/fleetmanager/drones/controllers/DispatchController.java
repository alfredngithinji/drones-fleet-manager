package com.musala.fleetmanager.drones.controllers;

import com.musala.fleetmanager.drones.dto.DroneLoadRequest;
import com.musala.fleetmanager.drones.dto.DroneLoadResponse;
import com.musala.fleetmanager.drones.dto.DroneRegistrationRequest;
import com.musala.fleetmanager.drones.dto.DroneRegistrationResponse;
import com.musala.fleetmanager.drones.services.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/drones")
public class DispatchController {
    @Autowired
    private DroneService droneService;

    @PostMapping(path = "/register", consumes = "application/json", produces = "application/json")
    public ResponseEntity<DroneRegistrationResponse> registerDrone(
            @Valid @RequestBody DroneRegistrationRequest droneRegistrationRequest) {
        DroneRegistrationResponse droneRegistrationResponse = droneService.droneRegistration(droneRegistrationRequest);

        return new ResponseEntity<DroneRegistrationResponse>(droneRegistrationResponse, HttpStatus.CREATED);

    }

    @PostMapping(path = "/load")
    public ResponseEntity<DroneLoadResponse> loadDrone(@Valid @RequestBody DroneLoadRequest droneLoadRequest){
        DroneLoadResponse droneLoadResponse=droneService.loadDrone(droneLoadRequest);
        return new ResponseEntity<>(droneLoadResponse,HttpStatus.ACCEPTED);
    }
}