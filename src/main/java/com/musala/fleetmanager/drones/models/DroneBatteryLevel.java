package com.musala.fleetmanager.drones.models;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class DroneBatteryLevel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Drone drone;

    @Column(nullable = false)
    private Integer batteryLevel;


    @Column(nullable = false)
    private LocalDateTime createdAt;
}
