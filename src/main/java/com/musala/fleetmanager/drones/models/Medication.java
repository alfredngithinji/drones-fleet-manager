package com.musala.fleetmanager.drones.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Float weight;

    @Column(nullable = false)
    private String code;

    @Column(nullable = false, columnDefinition = "text")
    private String image;
}
