package com.musala.fleetmanager.drones.models;

import com.musala.fleetmanager.drones.utils.DroneModelEnum;
import com.musala.fleetmanager.drones.utils.DroneStateEnum;
import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;

@Entity
@Data
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100,nullable = false)
    private String serialNumber;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DroneModelEnum model;

    @Column(nullable = false)
    private Float weight;

    @Column(nullable = false)
    private Integer batteryCapacity;

    @Column(nullable = false)
    @Valid()
    @Enumerated(EnumType.STRING)
    private DroneStateEnum state;
}
