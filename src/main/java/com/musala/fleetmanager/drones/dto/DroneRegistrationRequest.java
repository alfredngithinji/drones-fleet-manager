package com.musala.fleetmanager.drones.dto;

import com.musala.fleetmanager.drones.utils.DroneModelEnum;
import com.musala.fleetmanager.drones.utils.DroneStateEnum;
import com.musala.fleetmanager.drones.validator.ValidEnum;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class DroneRegistrationRequest {

    @NotNull
    @NotBlank
    @Size(max = 100)

    private String serialNumber;

    @ValidEnum(enumClass = DroneModelEnum.class)
    public  String model;

    @NotNull
    @NotBlank
    @Max(500)
    private Float weight;

    @NotNull
    @NotBlank
    private Integer batteryCapacity;


    @ValidEnum(enumClass = DroneStateEnum.class)
   String state;

}
