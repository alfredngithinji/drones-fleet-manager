package com.musala.fleetmanager.drones.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class MedicationDispatchItemRequest {

    @NotNull
    @NotBlank
    private Long droneId;

    @NotNull
    @NotBlank
    private Long medicationid;
}
