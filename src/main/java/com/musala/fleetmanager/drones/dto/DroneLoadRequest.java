package com.musala.fleetmanager.drones.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class DroneLoadRequest {
    @NotNull
    @NotBlank
    private String serialNumber;

    @NotBlank
    @NotNull
    private  String source;

    @NotNull
    @NotBlank
    private String destination;

    @NotBlank
    @NotNull
    private String code;
}
