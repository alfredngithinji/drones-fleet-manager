package com.musala.fleetmanager.drones.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DroneRegistrationResponse {

    private String serialNumber;
    private String message;
    private String result;
    private LocalDateTime createAt;
}
