package com.musala.fleetmanager.drones.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DroneLoadResponse {
    private String result;
    private String serialNumber;
    private String message;
    private LocalDateTime timestamp;
}
