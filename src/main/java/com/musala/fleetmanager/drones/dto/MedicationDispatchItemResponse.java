package com.musala.fleetmanager.drones.dto;

import lombok.Data;

@Data
public class MedicationDispatchItemResponse {
    private String message;

    private Float totalWeightAdded;

    private Float totalWeightRemaining;
}
