package com.musala.fleetmanager.drones.mappers;

import com.musala.fleetmanager.drones.dto.DroneRegistrationResponse;
import com.musala.fleetmanager.drones.models.Drone;
import org.mapstruct.Mapper;

@Mapper
public interface DroneMapper {
   DroneRegistrationResponse droneToDroneRegistrationResponse(Drone drone);

}
