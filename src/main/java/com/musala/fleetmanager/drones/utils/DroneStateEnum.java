package com.musala.fleetmanager.drones.utils;

public enum DroneStateEnum {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
