package com.musala.fleetmanager.drones.utils;

public enum DroneModelEnum {
    Lightweight, Middleweight, Cruiserweight, Heavyweight
}
