package com.musala.fleetmanager.drones.repository;

import com.musala.fleetmanager.drones.models.Drone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DroneRepository extends JpaRepository<Drone,Long> {
    Optional<Drone> findById(Long id);
    Optional<Drone> findBySerialNumber(String serialNumber);
}
