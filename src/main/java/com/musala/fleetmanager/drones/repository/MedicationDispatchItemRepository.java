package com.musala.fleetmanager.drones.repository;

import com.musala.fleetmanager.drones.models.MedicationDispatchItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationDispatchItemRepository extends JpaRepository<MedicationDispatchItem,Long> {
}
