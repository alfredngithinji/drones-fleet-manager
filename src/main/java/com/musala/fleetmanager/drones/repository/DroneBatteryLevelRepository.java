package com.musala.fleetmanager.drones.repository;

import com.musala.fleetmanager.drones.models.Drone;
import com.musala.fleetmanager.drones.models.DroneBatteryLevel;
import com.musala.fleetmanager.drones.models.MedicationDispatchItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DroneBatteryLevelRepository extends JpaRepository<DroneBatteryLevel, Long> {

    DroneBatteryLevel findFirstByDroneOrderByIdDesc(Drone drone);
}
