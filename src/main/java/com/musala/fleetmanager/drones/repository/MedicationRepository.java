package com.musala.fleetmanager.drones.repository;

import com.musala.fleetmanager.drones.models.Drone;
import com.musala.fleetmanager.drones.models.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MedicationRepository extends JpaRepository<Medication,Long> {
    Optional<Medication> findByCode(String code);
}
